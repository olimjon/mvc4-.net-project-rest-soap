﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WAD_CW2_00002399.Core.Entities;
using WAD_CW2_00002399.Web.Models;
using WAD_CW2_00002399.Web.Models.DTO;

namespace WAD_CW2_00002399.Web.Mappers
{
    //mapper is need for map the DB object model to view model
    public static class ProductMapper
    {
        public static ProductViewModel MapProductToModel(Product product)
        {
            return new ProductViewModel
            {
                Id = product.Id,
                ProductModel = product.ProductModel,
                Price = product.Price,
                Description = product.Description,
                ProductCategory = product.ProductCategory
            };
        }
        public static Product MapProductFromModel(ProductViewModel model)
        {
            return new Product
            {
                Id = model.Id,
                ProductModel = model.ProductModel,
                Price = model.Price,
                Description = model.Description,
                ProductCategory = model.ProductCategory
            };
        }
        public static List<ProductViewModel> MapProductsToModels(List<Product> list)
        {
            return list.Select(p => new ProductViewModel
            {
                Id = p.Id,
                ProductModel = p.ProductModel,
                ProductCategory = p.ProductCategory,
                Price = p.Price
            }).ToList();
        }

        public static List<ProductDTO> MapProductsToDTOs(List<Product> list)
        {
            return list.Select(p => new ProductDTO
            {
                Id = p.Id,
                ProductModel = p.ProductModel,
                Price = p.Price,
                Description = p.Description,
                ProductCategory = p.ProductCategory
            }).ToList();
        }

        public static Product MapProductFromDTO(ProductDTO model)
        {
            return new Product
            {
                Id = model.Id,
                ProductModel = model.ProductModel,
                Price = model.Price,
                Description = model.Description,
                ProductCategory = model.ProductCategory
            };
        }

        public static ProductDTO MapProductToDTO(Product product)
        {
            return new ProductDTO
            {
                Id = product.Id,
                ProductModel = product.ProductModel,
                Price = product.Price,
                Description = product.Description,
                ProductCategory = product.ProductCategory
            };
        }
    }
}