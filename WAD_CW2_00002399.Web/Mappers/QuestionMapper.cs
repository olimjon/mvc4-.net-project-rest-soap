﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WAD_CW2_00002399.Core.Entities;
using WAD_CW2_00002399.Core.Services;
using WAD_CW2_00002399.Web.Models;

namespace WAD_CW2_00002399.Web.Mappers
{
    public class QuestionMapper
    {
        public static Questions MapQuestionsFromModel(QuestionViewModel model)
            
        {

            QuestionsDBService db = new QuestionsDBService();

            return new Questions
            {
                
                Id = model.Id,
                Name = model.Name,
                Email = model.Email,
                Category = model.Category,
                //QuestionCategory = db.GetQuestionCats().Where(x => x.Id == model.Category).FirstOrDefault(),
                Question = model.Question
                
            };
        }
    }
}