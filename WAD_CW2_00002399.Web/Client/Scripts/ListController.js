﻿(function (app) {
    //create dynamic variable that is a function
    //see how $scope and $http are injected to the function
    var ListController = function ($scope, $http) {
        //get data from the endpoint
        $http.get("/api/ExternalAccess/Users")
            .success(function (data) {
                $scope.users = data;
            });
    };
    //add data to the controller object
    app.controller("ListController", ListController);
}(angular.module("users")))