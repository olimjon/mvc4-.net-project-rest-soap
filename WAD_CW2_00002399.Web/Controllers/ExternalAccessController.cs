﻿using WAD_CW2_00002399.Core.Entities;
using WAD_CW2_00002399.Core.Services;
using WAD_CW2_00002399.Web.Mappers;
using WAD_CW2_00002399.Web.Models;
using WAD_CW2_00002399.Web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WAD_CW2_00002399.Web.Controllers
{
    public class ExternalAccessController : ApiController
    {
        //static properties does not requiere instantiation on accessing different Actions
        static IProductService _productService;
        static UserService _userService = new UserService();
        //singleton to ensure that there is only one instance of the service
        private IProductService productService
        {
            get
            {
                if (_productService == null)
                    _productService = new ProductDBService();
                return _productService;
            }
        }

        [HttpGet]
        [ActionName("Message")]
        public object GetMessage()
        {
            return new  {
                         message = "Avtech!"
            };
        }

        [HttpGet]
        [ActionName("CountProducts")]
        public object GetProductsCount()
        {
           return new { count = productService.GetAll().Count() };
        }

        [HttpGet]
        [ActionName("Products")]
        public List<ProductDTO> GetProducts()
        {
            return ProductMapper.MapProductsToDTOs(productService.GetAll());
        }

        [HttpGet]
        [ActionName("Product")]
        public ProductDTO GetProduct(int id)
        {
            return ProductMapper.MapProductToDTO(productService.GetById(id));
        }
        

        [HttpDelete]
        [ActionName("Product")]
        public void DeleteProduct(int id)
        {
            productService.Delete(id);
        }

        [HttpPost]
        [ActionName("Authenticate")]
        public object Login([FromBody] UserDTO user)
        {
            if (user == null)
                return new
                {
                    isSuccess = false,
                    Message = "Empty username or password",
                    Token = ""
                };
            var token = _userService.APIAuthenticate(user.Username, user.Password);
            if (token == string.Empty)
                return new
                {
                    isSuccess = false,
                    Message = "Wrong username or password",
                    Token = ""
                };

            return new
            {
                isSuccess = true,
                Message = "Autorized. Your token is: " + token,
                Token = token

            };
        }

        [HttpPost]
        [ActionName("Product")]
        public object PutProduct([FromBody] ProductDTO product, string token)
        {
            if (!_userService.APIAuthenticate(token))
                return new
                {
                    isSuccess = false,
                    Message = "Wrong token"
                };
            productService.Insert(ProductMapper.MapProductFromDTO(product));

            return new
            {
                isSuccess = true,
                Message = "New product added successfully"
            };

        }


        [HttpGet]
        [ActionName("Users")]
        public object Users()
        {
            var userService = new UserService();
            return userService.GetAllUsers();
        }


    }
}
