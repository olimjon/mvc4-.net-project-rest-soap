﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WAD_CW2_00002399.Core.Services;
using WAD_CW2_00002399.Web.Mappers;
using WAD_CW2_00002399.Web.Models;

namespace WAD_CW2_00002399.Web.Controllers
{
    public class SupportController : BaseController
    {
        //static properties does not requiere instantiation on accessing different Actions
        static IQuestionsService _questionService;

        //singleton to ensure that there is only one instance of the service
        private IQuestionsService questionService
        {
            get
            {
                if (_questionService == null)
                    _questionService = new QuestionsDBService();
                return _questionService;
            }
        }
        // GET: Support
        public ActionResult Index()
        {
            var model = new QuestionViewModel();
            ViewBag.Category = new SelectList(questionService.GetQuestionCats(), "Id", "Name", 1);
            return View();
        }

        [HttpPost]
        public ActionResult Index(QuestionViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var question = QuestionMapper.MapQuestionsFromModel(model);
                    questionService.Insert(question);
                    ViewBag.Category = new SelectList(questionService.GetQuestionCats(), "id", "name", model.Id);
                    var cat = question.QuestionCategory.Name;

                    var Smtp = new SmtpClient("smtp.mail.ru", 25);
                    Smtp.Credentials = new NetworkCredential("00002399@mail.ru", "chunga-changa"); //lets say this is mail server
                    Smtp.EnableSsl = true;

                    //Message formating
                    System.Net.Mail.MailMessage Message = new System.Net.Mail.MailMessage();
                    //From 
                    Message.From = new MailAddress("00002399@mail.ru");//email from should be equal to the mail server
                    //To
                    Message.To.Add(new MailAddress("supprts.mail.address@mail.ru"));//mail address will be sent to support stuff's mail
                    Message.Subject = "Subject: " + cat;
                    string message = "From: " + model.Email + " ";
                    message = message + "\n";

                    message = message + "User name: " + model.Name;

                    message = message + "\n";

                    message = message + "Question: " + "\n" + model.Question;

                    Message.Body = message;

                    Smtp.Send(Message);//sending

                    ViewBag.successMessage = "Thank you for your question! The support staff will connect with you soon.";
                }
                catch (Exception e)
                {
                    ViewBag.successMessage = "An error has occured. Please try again later " + e.Message;
                }
            }
            else
            {
                throw new Exception("Хакер ака пожалуйста введите значиния в поля. Я тут 15 часов без перерыва вожусь с валидейшоном. До questionViewModel не доходит и никакой validation не срабатывает и передает null значения ((( и сейчас 3 часа ночи");
            }
            return View(model);
        }
    }
}