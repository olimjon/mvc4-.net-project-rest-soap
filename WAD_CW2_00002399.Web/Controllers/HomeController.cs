﻿using WAD_CW2_00002399.Web.Models;
using WAD_CW2_00002399.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using WAD_CW2_00002399.Web.Mappers;
using WAD_CW2_00002399.Core.Services;

namespace WAD_CW2_00002399.Web.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }        
    }
}