﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WAD_CW2_00002399.Core.Services;
using WAD_CW2_00002399.Web.Mappers;
using WAD_CW2_00002399.Web.Models;
using WAD_CW2_00002399.Web.Helpers;
using System.Xml.Linq;
using System.IO;
using System.Xml.Schema;

namespace WAD_CW2_00002399.Web.Controllers
{
    public class ProductController : BaseController
    {
        //static properties does not requiere instantiation on accessing different Actions
        static IProductService _productService;

        //singleton to ensure that there is only one instance of the service
        private IProductService productService {
            get
            {
                if (_productService == null)
                    _productService = new ProductDBService();
                return _productService;
            } }

        // GET: Product
        public ActionResult Index()
        {
            var list = ProductMapper.MapProductsToModels(productService.GetAll());
            return View(list);
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            var model = ProductMapper.MapProductToModel(productService.GetById(id));
            return View(model);
        }
        public ActionResult Catalog(ProductCatalogViewModel model, int?page)
        {
            //should send model to the view
            var catalog = new ProductCatalogViewModel();
            catalog.Products = ProductMapper.MapProductsToModels(productService.GetAll());

            //if the search for was submitted
            if (model != null)
            {
                //map the sent params to created model
                catalog.SearchOption = model.SearchOption;
                catalog.SearchQuery = model.SearchQuery;
                catalog.SortOption = model.SortOption;
                catalog.SortOrder = model.SortOrder;


                //searching
                if (!String.IsNullOrEmpty(catalog.SearchQuery) && !String.IsNullOrWhiteSpace(catalog.SearchQuery))
                {
                    if (catalog.SearchOption == ProductCatalogSearchOption.Model)
                        catalog.Products = catalog.Products.Where(p => p.ProductModel.ToLower().Contains(catalog.SearchQuery.ToLower())).ToList();
                    else
                        catalog.Products = catalog.Products.Where(p => p.Description.ToLower().Contains(catalog.SearchQuery.ToLower())).ToList();
                }

                //sorting
                if(catalog.SortOption == ProductCatalogSortOption.Model)
                {
                    if (catalog.SortOrder == ProductCatalogSortOrder.Ascending)
                        catalog.Products = catalog.Products.OrderBy(p => p.ProductModel).ToList();
                    else
                        catalog.Products = catalog.Products.OrderByDescending(p => p.Price).ToList();
                }
                else
                {
                    if (catalog.SortOrder == ProductCatalogSortOrder.Ascending)
                        catalog.Products = catalog.Products.OrderBy(p => p.Price).ToList();
                    else
                        catalog.Products = catalog.Products.OrderByDescending(p => p.Price).ToList();
                }
            }

            int pageSize = 6;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            catalog.TotalPages = catalog.Products.Count / pageSize;
            catalog.Products = catalog.Products.Skip(pageSize * (pageIndex - 1)).Take(pageSize).ToList();

            return View(catalog);
        }
        // GET: Product/Create
        public ActionResult Create()
        {
            var model = new ProductViewModel();
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var product = ProductMapper.MapProductFromModel(model);
                    productService.Insert(product);
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            var model = ProductMapper.MapProductToModel(productService.GetById(id));
            return View(model);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, ProductViewModel model)
        {
            model.Id = id;
            try
            {
                if (ModelState.IsValid)
                {
                    var product = ProductMapper.MapProductFromModel(model);
                    productService.Update(product);
                    return RedirectToAction("Index");
                }
                return View(model);

            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            var model = ProductMapper.MapProductToModel(productService.GetById(id));
            return View(model);
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                productService.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Export(string type)
        {
            var xDoc = new XDocument();
            switch (type)
            {
                case "html":
                    xDoc.Add(new XProcessingInstruction("xml-stylesheet", "type='text/xsl' href='/xml/ProductToHTML.xslt'"));
                    break;
                case "csv":
                    xDoc.Add(new XProcessingInstruction("xml-stylesheet", "type='text/xsl' href='/xml/ProductToCSV.xslt'"));
                    break;
                default:
                    break;
            }
            xDoc.Declaration = new XDeclaration("1.0", "utf-8", null);
            var products = productService.GetAll();
            if (products.Count > 0)
            {
                var xElement = new XElement("Products",
                    from product in products
                    select new XElement("Product",
                        new XElement("Id", product.Id),
                        new XElement("ProductModel", product.ProductModel),
                        new XElement("Description", product.Description),
                        new XElement("ProductCategory", product.ProductCategory),
                        new XElement("Price", product.Price),
                        new XElement("Created", product.Created),
                        product.Updated.HasValue ? new XElement("Updated", product.Updated) : null
                        ));
                xDoc.Add(xElement);
            }

            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add("", "http://" + System.Web.HttpContext.Current.Request.Url.Authority + "/xml/ProductSchema.xsd");
            var errors = false;
            var eMessage = "";
            xDoc.Validate(schemas, (o, e) =>
            {
                eMessage = e.Message;
                errors = true;
            }, true);
            if (errors)
                xDoc = new XDocument(new XDeclaration("1.0", "utf-8", null), new XElement("Error", eMessage));

            StringWriter sw = new StringWriter();

            xDoc.Save(sw);
            var context = System.Web.HttpContext.Current;
            context.Response.Clear();
            context.Response.Write(sw.ToString());
            context.Response.ContentType = "text/xml";
            context.Response.End();
            return View();
        }
    }
}
