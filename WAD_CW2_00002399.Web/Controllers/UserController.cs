﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WAD_CW2_00002399.Core.Entities;
using WAD_CW2_00002399.Core.Services;
using WAD_CW2_00002399.Web.Helpers;
using WAD_CW2_00002399.Web.Models;

namespace WAD_CW2_00002399.Web.Controllers
{
    public class UserController : BaseController
    {
        static UserService _userService = new UserService();

        // GET: User/Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: User/Login
        [HttpPost]
        public ActionResult Login(LoginViewModel loginVM)
        {
            if (!ModelState.IsValid)
                return View(loginVM);
            try
            {
                var curUser = new Users
                {
                    Username_db = loginVM.UserName,
                    Password_db = loginVM.Password
                };
                if (_userService.Authenticate(curUser).HasValue)
                { 
                    //FormsAuthentication.SetAuthCookie(curUser.Username_db, true);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid credentials");
                    return View(loginVM);
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(loginVM);
            }
        }

        //public ActionResult LogOut()
        //{
        //    FormsAuthentication.SignOut();
        //    return RedirectToAction("Index", "Home");
        //}

        // GET: User/Register
        public ActionResult Registration()
        {
            return View();
        }

        // POST: User/Register
        [HttpPost]
        public ActionResult Registration(RegViewModel registrationVM)
        {
            if (!ModelState.IsValid)
                return View(registrationVM);
            var valid = ValidateCaptcha();
            if (!valid)
            {
                ViewBag.Message = "Invalid Capture";
                return View();
            }
            try
            {
                var curUser = new Users
                {
                    Username_db = registrationVM.UserName,
                    FirstName_db = registrationVM.FirstName,
                    LastName_db = registrationVM.LastName,
                    Address_db = registrationVM.Address,
                    Password_db = registrationVM.Password,
                    Email_db = registrationVM.Email
                };
                _userService.Register(curUser);
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(registrationVM);
            }
        }

        public ActionResult Administration()
        {
            return View();
        }

        private Boolean ValidateCaptcha()
        {
            var response = Request["g-recaptcha-response"];
            //secret that was generated in key value pair
            string secret = ConfigurationManager.AppSettings["reCAPTCHASecretKey"];
            var client = new WebClient();
            var reply =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));
            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
            //when response is false check for the error message
            if (!captchaResponse.Success)
            {
                if (captchaResponse.ErrorCodes.Count <= 0) return false;
                var error = captchaResponse.ErrorCodes[0].ToLower();
                switch (error)
                {
                    case ("missing-input-secret"):
                        ViewBag.message = "The secret parameter is missing.";
                        break;
                    case ("invalid-input-secret"):
                        ViewBag.message = "The secret parameter is invalid or malformed.";
                        break;
                    case ("missing-input-response"):
                        ViewBag.message = "The response parameter is missing. Please, preceed with reCAPTCHA.";
                        break;
                    case ("invalid-input-response"):
                        ViewBag.message = "The response parameter is invalid or malformed.";
                        break;
                    default:
                        ViewBag.message = "Error occured. Please try again";
                        break;
                }
                return false;
            }
            ViewBag.message = "Valid";
            return true;
        }
    }

}