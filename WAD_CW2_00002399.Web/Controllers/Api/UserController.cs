﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WAD_CW2_00002399.Core.Services;

namespace WAD_CW2_00002399.Web.Controllers.Api
{
    public class UserController : ApiController
    {
        private UserService _userService;
        public UserController()
        {
            _userService = new UserService();
        }

        [HttpGet]
        [ActionName("Users")]
        public object Users()
        {
            return _userService.GetAllUsers();
        }
        [HttpGet]
        [ActionName("UserNameExists")]
        public object UserNameExists(string userName)
        {
            if (userName == null)
                return BadRequest("No UserName provided");
            var doesExist = _userService.UserNameExists(userName);
            return doesExist;
        }

    }
}
