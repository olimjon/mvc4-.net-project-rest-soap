﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="iso-8859-1"/>
  <xsl:strip-space elements="*" />
  <xsl:template match="/">
    <xsl:for-each select="/Products/Product">
      <xsl:value-of select="Id" />, "<xsl:value-of select="ProductModel" />", "<xsl:value-of select="Description" />", "<xsl:value-of select="ProductCategory" />", "<xsl:value-of select="Price" />" , "<xsl:value-of select="Created" />", <xsl:if test="Updated">
        <xsl:value-of select="Updated" />
      </xsl:if><xsl:if test="not(Updated)">"Not specified"</xsl:if> <xsl:if test="position() != last()">
        <xsl:text>&#xD;</xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
