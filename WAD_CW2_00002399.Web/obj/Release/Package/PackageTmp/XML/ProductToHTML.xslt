﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <html>
      <head>
        <ProductModel>Products | WAD_CW2_00002399.Web</ProductModel>
      </head>
      <style>
        .even, thead {
        background-color:#ffffe0;
        }
        table {
        width:100%;
        }
        thead  {
        text-align:left;
        }
      </style>
      <body>
        <h1>WAD_CW2_00002399.Web Product List</h1>
        <table>
          <thead>
            <th>ID</th>
            <th>ProductModel</th>
            <th>Description</th>
            <th>ProductCategory</th>
            <th>Price</th>
            <th>Created</th>
            <th>Updated</th>
          </thead>
          <tbody>
            <xsl:for-each select="/Products/Product">
              <tr>
                <xsl:attribute name="class">
                  <xsl:choose>
                    <xsl:when test="(position() mod 2 != 1)">
                      <xsl:text>even</xsl:text>
                    </xsl:when>
                  </xsl:choose>
                </xsl:attribute>
                <td>
                  <xsl:value-of select="Id" />
                </td>
                <td>
                  <xsl:value-of select="ProductModel" />
                </td>
                <td>
                  <xsl:value-of select="Description" />
                </td>
                <td>
                  <xsl:value-of select="ProductCategory" />
                </td>
                <td>
                  <xsl:value-of select="Price" />
                </td>
                <td>
                  <xsl:value-of select="Created" />
                </td>
                <td>
                  <xsl:if test="Updated">
                    <xsl:value-of select="Updated" />
                  </xsl:if>
                  <xsl:if test="not(Updated)">Not specified</xsl:if>
                </td>
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
