﻿$(function () {
    var $products = $('#products');
    $.ajax({
        type: 'GET',
        url: 'http://localhost:51346/api/ExternalAccess/Products', //Please put correct localhost port in order to run it
        success: function (products) {
            $.each(products, function (i, pro) {
                $products.append('<li><b>Model:</b> ' + pro.ProductModel + '<b> Description: </b>' + pro.Description + ' <b>Category: </b>' + pro.ProductCategory + '<b> Price: </b>' + pro.Price + '</li>');
            });
        },
        error: function () {
            alert('error loading product list')
        }
    });
    
    var $model = $('#model');
    var $description = $('#description');
    var $category = $('#category');
    var $price = $('#price');
    $('#btnAdd').on('click', function () {

        var pros = {
            ProductModel: $model.val(),
        Description: $description.val(),
        ProductCategory: $category.val(),
        Price: $price.val()
    };

    $.ajax({
        type: 'POST',
        url: 'http://localhost:51346/api/ExternalAccess/Product',
        data: pros,
        success: function(newPro) {
            $products.append('<li><b>Model:</b> ' + newPro.ProductModel + '<b> Description: </b>' + newPro.Description + ' <b>Category: </b>' + newPro.ProductCategory + '<b> Price: </b>' + newPro.Price + '</li>');
        },
        error: function() {
            alert('error saving of product')
        }
    })
});
});