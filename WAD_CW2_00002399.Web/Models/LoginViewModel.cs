﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WAD_CW2_00002399.Web.Models
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}