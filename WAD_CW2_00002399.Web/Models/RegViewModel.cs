﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WAD_CW2_00002399.Web.Models
{
    public class RegViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}