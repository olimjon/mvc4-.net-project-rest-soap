﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WAD_CW2_00002399.Web.Models
{
    public class QuestionViewModel
    {

        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Your Name")]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Your email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Category of question")]
        public int Category { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Your question")]
        public string Question { get; set; }
    }
}