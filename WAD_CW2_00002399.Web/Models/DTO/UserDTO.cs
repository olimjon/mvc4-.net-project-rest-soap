﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WAD_CW2_00002399.Web.Models.DTO
{
    [DataContract]
    public class UserDTO
    {
        [DataMember(IsRequired = true)]
        public string Username { get; set; }
        [DataMember(IsRequired = true)]
        public string Password { get; set; }
    }

}