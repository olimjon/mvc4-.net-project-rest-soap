﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WAD_CW2_00002399.Web.Models.DTO
{
    [DataContract]
    public class ProductDTO
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string ProductModel { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string ProductCategory { get; set; }
        [DataMember]
        public double Price { get; set; }
    }
}