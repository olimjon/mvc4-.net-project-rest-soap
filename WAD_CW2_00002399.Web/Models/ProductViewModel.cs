﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WAD_CW2_00002399.Web.Models
{

    //View models are only for the web site part
    //Should be seperate from models that represents entities in DB
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(maximumLength: 100, MinimumLength = 1)]
        [Display(Name = "Model")]
        public string ProductModel { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:#,###0.00} UZS")]
        [Display(Name = "Price")]
        public double Price { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Category")]
        public string ProductCategory { get; set; }
    }
}