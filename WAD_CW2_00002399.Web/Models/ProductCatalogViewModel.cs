﻿using WAD_CW2_00002399.Web.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WAD_CW2_00002399.Web.Models
{
    public class ProductCatalogViewModel
    {
        public ProductCatalogSortOption SortOption { get; set; }
        public ProductCatalogSortOrder SortOrder { get; set; }
        public ProductCatalogSearchOption SearchOption { get; set; }

        public string SearchQuery { get; set; }
        public List<ProductViewModel> Products { get; set; }

        public int TotalPages { get; set; }
    }
}