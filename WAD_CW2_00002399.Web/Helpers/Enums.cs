﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WAD_CW2_00002399.Web.Helpers
{
    public enum ProductCatalogSortOption
    {
        Model = 0,
        Price = 1
    }
    public enum ProductCatalogSortOrder
    {
        Ascending = 0,
        Descending = 1
    }
    public enum ProductCatalogSearchOption
    {
        Model = 0,
        Description = 1
    }
}