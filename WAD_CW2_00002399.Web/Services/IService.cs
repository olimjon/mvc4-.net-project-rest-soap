﻿using WAD_CW2_00002399.Web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WAD_CW2_00002399.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService" in both code and config file together.
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        List<ProductDTO> GetProducts();

        [OperationContract]
        ProductDTO GetProduct(int id);

        [OperationContract]
        void UpdateProduct(ProductDTO product);
        [OperationContract]
        void InsertProduct(ProductDTO product);
        [OperationContract]
        void DeleteProduct(int id);

    }
}
