﻿using WAD_CW2_00002399.Core.Services;
using WAD_CW2_00002399.Web.Mappers;
using WAD_CW2_00002399.Web.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WAD_CW2_00002399.Web.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service : IService
    {
        IProductService productService = new ProductDBService();


        public List<ProductDTO> GetProducts()
        {

            return ProductMapper.MapProductsToDTOs(productService.GetAll());
        }

        public ProductDTO GetProduct(int id)
        {

            return ProductMapper.MapProductToDTO(productService.GetById(id)); ;
        }

        public void UpdateProduct(ProductDTO product)
        {
            productService.Update(ProductMapper.MapProductFromDTO(product));
        }

        public void InsertProduct(ProductDTO product)
        {
            productService.Insert(ProductMapper.MapProductFromDTO(product));
        }


        public void DeleteProduct(int id)
        {
            productService.Delete(id);
        }
    }
}
