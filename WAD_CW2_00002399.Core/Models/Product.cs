﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAD_CW2_00002399.Core.Models
{
    //model represents the object stored in memory
    public class Product
    {
        public long Id { get; set; }
        public string ProductModel { get; set; }
        public string ProductCategory { get; set; }
        public double Price { get; set; }

        //parameters could be different from view model (depends on the business case)
        //assuming that there should be also stored the date of creation and date of last update
        public DateTime DateCreated { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}
