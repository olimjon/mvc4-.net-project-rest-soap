﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAD_CW2_00002399.Core.Entities;

namespace WAD_CW2_00002399.Core.Services
{
    //service for storing data in the memory
    //contains CRUD methods for DB objects
    public  class ProductMemoryService : IProductService
    {
        //where the actual data is stored
        private List<Product> Products;

        public ProductMemoryService()
        {
            // create new list of product on instantiation
            Products = new List<Product>();
        }

        public void Delete(int itemId)
        {
            Products.RemoveAll(x => x.Id == itemId);
        }

        public List<Product> GetAll()
        {
            return Products.ToList();
        }

        public Product GetById(int id)
        {
            return Products.First(x => x.Id == id);
        }

        public void Insert(Product item)
        {
            if (item == null)
                throw new ArgumentNullException("You cannot insert a null product");
            //should define the Id property manually
            var last = Products.Max(x => (int?)x.Id) ?? 0;
            item.Id = last + 1;
            item.Created = DateTime.Now;
            Products.Add(item);
        }

        public void Update(Product item)
        {
            if (item == null)
                throw new ArgumentNullException("You cannot update a null product");
            var target = Products.FirstOrDefault(x => x.Id == item.Id);
            target.ProductModel = item.ProductModel;
            target.ProductCategory = item.ProductCategory;
            target.Price = item.Price;
            target.Updated = DateTime.Now;
        }
    }
}
