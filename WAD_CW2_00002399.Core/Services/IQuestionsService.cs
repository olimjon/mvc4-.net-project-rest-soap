﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAD_CW2_00002399.Core.Entities;

namespace WAD_CW2_00002399.Core.Services
{
    public interface IQuestionsService
    {
        void Insert(Questions item);
        List<Questions> GetAll();
        List<QuestionCategory> GetQuestionCats();
    }
}
