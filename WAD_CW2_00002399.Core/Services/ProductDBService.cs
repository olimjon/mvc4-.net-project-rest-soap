﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAD_CW2_00002399.Core.Entities;

namespace WAD_CW2_00002399.Core.Services
{
    public class ProductDBService : IProductService
    {
        private DBEntities db;
        public ProductDBService()
        {
            db = new DBEntities();
        }
        public void Delete(int itemId)
        {
            var item = db.Product.FirstOrDefault(p => p.Id == itemId);
            if (item == null)
                throw new ArgumentNullException("You cannot delete a null product");
            db.Product.Remove(item);
            db.SaveChanges();
        }

        public List<Product> GetAll()
        {
            return db.Product.ToList();
        }

        public Product GetById(int id)
        {
            var item = db.Product.FirstOrDefault(p => p.Id == id);
            if (item == null)
                throw new ArgumentNullException("Product does not exists");
            return item;
        }

        public void Insert(Product item)
        {
            //log the date time created
            item.Created = DateTime.Now;
            db.Product.Add(item);
            db.SaveChanges();
        }

        public void Update(Product item)
        {
            var dbItem = db.Product.FirstOrDefault(p => p.Id == item.Id);
            if (dbItem == null)
                throw new ArgumentNullException("Product does not exists");
            dbItem.ProductModel = item.ProductModel;
            dbItem.Description = item.Description;
            dbItem.ProductCategory = item.ProductCategory;
            dbItem.Price = item.Price;
            //log the date time updated
            dbItem.Updated = DateTime.Now;
            db.SaveChanges();
        }
    }
}
