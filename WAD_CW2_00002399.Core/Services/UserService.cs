﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WAD_CW2_00002399.Core.Entities;

namespace WAD_CW2_00002399.Core.Services
{
    public class UserService
    {
        static private DBEntities database;
        public UserService()
        {
            database = new DBEntities();
        }

        public int? Authenticate(Users user)
        {
            var curHashedPassword = BuildPassword(user.Username_db, user.Password_db);
            var curUser = database.Users.FirstOrDefault(u => u.Username_db == user.Username_db && u.Password_db == curHashedPassword);
            return curUser == null ? (int?)null : curUser.Id_db;

        }

        public bool APIAuthenticate(string token)
        {
            var user = database.Users.FirstOrDefault(u => u.APIToken_db == token);
            if (user == null)
                return false;

            return true;
        }

        public string APIAuthenticate(Users user)
        {
            var id = Authenticate(user);
            if (!id.HasValue)
                return string.Empty;
            var token = GenerateAPIToken(id.Value, user.Username_db, user.Password_db);
            database.Users.First(u => u.Id_db == id).APIToken_db = token;
            database.SaveChanges();
            return token;
        }

        public string APIAuthenticate(string username, string password)
        {
            var curUser = new Users
            {
                Username_db = username,
                Password_db = password,
            };
            return APIAuthenticate(curUser);
        }

        private string GenerateAPIToken(int id, string username, string password)
        {
            var toEncrypt = username + id + password;
            HashAlgorithm hash = new SHA256Managed();
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(toEncrypt);
            byte[] hashBytes = hash.ComputeHash(plainTextBytes);

            //in this string you got the encrypted password
            return Convert.ToBase64String(hashBytes);
        }

        private bool ValidateAPIToken(string token, int id, string username, string password)
        {
            return token == GenerateAPIToken(id, username, password);
        }




        /// <summary>
        ///algorithm for Hash with Salt
        ///but the Salt is dinamic - unique for each user instance
        ///assuming that username is constant and never be changed and length is at least 5 chars
        ///create the password string before encryption takes first 3 chars of login then pwd then last 3 chars of login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private string BuildPassword(string userName, string password)
        {
            if (userName.Length < 3)
                throw new Exception("Your UserName is less than 3 characters");
            var result = userName.Substring(0, 3) + password + userName.Substring(userName.Length - 3);
            return EncodeString(result);
        }

        private string EncodeString(string curString)
        {
            //Before you hash a string you parse it into a byte array
            var curByteArray = Encoding.UTF8.GetBytes(curString);
            using (var sha = new SHA256CryptoServiceProvider())
            {
                var curHashedByteArray = sha.ComputeHash(curByteArray);
                //not string type because of the performance
                var mutableString = new StringBuilder();
                foreach (var curHashedByte in curHashedByteArray)
                {
                    //puts some characters to the upper case
                    mutableString.Append(curHashedByte.ToString("x2"));
                }
                return mutableString.ToString();
            }
        }



        public static bool IsUnique(Users user)
        {
            return !database.Users.Any(u => u.Username_db == user.Username_db || u.Email_db == user.Email_db);
        }
        public void Register(Users user)
        {
            if (!IsUnique(user))
                throw new Exception("Email or Username already exist in the database");
            user.Password_db = BuildPassword(user.Username_db, user.Password_db);
            database.Users.Add(user);
            database.SaveChanges();
        }


        public IEnumerable<Users> GetAllUsers()
        {
            var users = database.Users.Where(u => u.Username_db != "");
            foreach (var user in users)
                user.Password_db = ":p I will not show passwords";
            return users;
        }

        public bool UserNameExists(string userName)
        {
            return database.Users.Any(u => u.Username_db == userName);
        }


    }

}
