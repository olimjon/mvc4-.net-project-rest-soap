﻿using WAD_CW2_00002399.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAD_CW2_00002399.Core.Services
{
    //an interface for further services
    //in case of impementation of new type of the service (i.e. db storage) app still would work
    //additional flexibility for the project
    public interface IProductService
    {
        void Insert(Product item);
        void Update(Product item);
        void Delete(int itemId);
        List<Product> GetAll();
        Product GetById(int id);
    }
}
