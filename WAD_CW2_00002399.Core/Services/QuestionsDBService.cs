﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAD_CW2_00002399.Core.Entities;

namespace WAD_CW2_00002399.Core.Services
{
    public class QuestionsDBService : IQuestionsService
    {
        private DBEntities qdb;
        public QuestionsDBService()
        {
            qdb = new DBEntities();
        }
        public List<Questions> GetAll()
        {
            return qdb.Questions.ToList();
        }

        public void Insert(Questions item)
        {
            qdb.Questions.Add(item);
            qdb.SaveChanges();
        }
        public List<QuestionCategory> GetQuestionCats()
        {
            return qdb.QuestionCategory.ToList();
        }

    }
}
