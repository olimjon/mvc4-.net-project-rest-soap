﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="REST_Client.aspx.cs" Inherits="WAD_CW2_00002399.WSClient.REST_Client" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #products {
            margin-left: 301px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ul id="products">
        <h1>Here product list should appear:</h1></ul>
        <h4>Add products</h4>

        <table style="font-family:Arial; border:1px solid black">
            <tr>
                <td>
                    <b>Model </b>
                </td>
                <td>
                    <asp:TextBox ID="model" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Description </b>
                </td>
                <td>
                    <asp:TextBox ID="description" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Category </b>
                </td>
                <td>
                    <asp:TextBox ID="category" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Price </b>
                </td>
                <td>
                    <asp:TextBox ID="price" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>

                </td>
                <td>
                    <b>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" />
                    </b>
                </td>
            </tr>
        </table>
    </div>
    </form>
    <script src="/JS/jquery-1.12.0.min.js"></script>
    <script src="/JS/Main.js"></script>
</body>
</html>
