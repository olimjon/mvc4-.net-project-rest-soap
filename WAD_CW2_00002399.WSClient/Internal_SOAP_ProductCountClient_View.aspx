﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Internal_SOAP_ProductCountClient_View.aspx.cs" Inherits="WAD_CW2_00002399.WSClient.Internal_SOAP_ProductCountClient_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <table style="font-family:Arial; border:1px solid black">
    <tr>
        <td>
            <b>Overall number of products:</b>
        </td>
        <td>
            <asp:Label ID="lblResult" runat="server"></asp:Label>
            <asp:Button ID="btnGetWeather" runat="server" Text="Count" 
                onclick="btnGetWeather_Click"/>
        </td>
    </tr>
</table>
    </form>
</body>
</html>
