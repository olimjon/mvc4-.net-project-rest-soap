﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WAD_CW2_00002399.WSClient
{
    public partial class External_SOAP_WeatherCLient_View : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetWeather_Click(object sender, EventArgs e)
        {
            External_SOAP_Weather.WeatherSoapClient client = new External_SOAP_Weather.WeatherSoapClient("WeatherSoap");
            External_SOAP_Weather.WeatherReturn result = client.GetCityWeatherByZIP(txtZip.Text);

            if (result.Success)
            {
                lblError.Text = string.Empty;
                lblCity.Text = result.City;
                lblState.Text = result.State;
                lblTemperature.Text = result.Temperature;
                lblWind.Text = result.Wind;
                lblWeatherStationCity.Text = result.WeatherStationCity;
            }
            else
            {
                lblError.Text = result.ResponseText;
                lblCity.Text = string.Empty;
                lblState.Text = string.Empty;
                lblTemperature.Text = string.Empty;
                lblWind.Text = string.Empty;
                lblWeatherStationCity.Text = string.Empty;
            }
        }
    }
}