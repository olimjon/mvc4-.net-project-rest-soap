﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WAD_CW2_00002399.WSClient
{
    public partial class Internal_SOAP_ProductCountClient_View : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetWeather_Click(object sender, EventArgs e)
        {
            Internal_SOAP_ProductCountClient.ServiceClient client = new Internal_SOAP_ProductCountClient.ServiceClient();
            var count = client.GetProducts().Count();
            lblResult.Text = count.ToString();
        }
    }
}